---
Title: "Start"
ordersectionsby: "weight"
---

# ![fischertechnik community](images/logo-full.png)


## Zur freundlichen Beachtung

Dies ist eine Testseite für die Technik (Scriptentwicklung) unserer ftc-Seite!
Die Produktivseite muss einige Inhalte zum Testen beisteuern, allerdings
sind diese Inhalte aufs Minimum reduziert.

Die Rubrik "Website (Technik)" hier ist einigermassen ernst zu nehmen,
diese Inhalte sind für die Wartungsarbeiter und Admins gedacht.
Solange sie allerdings auf der
[Produktivseite](https://ftcommunity.de/techdoc/)
zu finden sind, gelten ausschließlich die dortigen Inhalte! Hier mögen sie
etwas veraltet sein weil die neuesten Versionen nur hin und wieder kopiert
werden.
