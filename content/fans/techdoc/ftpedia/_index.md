---
title: "ft:pedia"
layout: "techdoc"
weight: 30
stand: "28. Oktober 2019"
---

In der _section_ "ft:pedia" werden die Inhalte unserer geliebten
Fachzeitschrift bereitgestellt. Dabei wird im Repo in zwei Bereiche
unterschieden:

*  Inhalte
*  Technik

Die Seiten der _section_ "ft:pedia" werden durch einige Scripte (Technik) aus
den vorgefundenen Inhalten gebaut.

Außer der _section_ und ihrer Hauptseite gibt es noch die Untersektionen
'Jahrgang' und 'Ausgabe'.

Bevor jetzt die jeweilige Bechreibung kommt, sollten noch ein paar
Feinheiten erwähnt werden:

*  In jedem Ordner der Inhalte gibt es eine Datei `_index.md`.
*  Hugo wendet das Script
   [`themes/website-layout/layouts/ftpedia/list.html`](ftpedia-list-html)
   auf jedes vorhandene `_index.md` in und unterhalb `content/ftpedia/` an.
*  Sowohl die Übersichtsseite als auch jeder 'Jahrgang' und jede 'Ausgabe'
   werden durch jeweils ein eigenes `_index.md` beschrieben.
*  Alle Seitentypen in der _section_ `ftpedia/` müssen vom
   [`themes/website-layout/layouts/ftpedia/list.html`](ftpedia-list-html)
   Script unterschieden und entsprechend behandelt werden.
*  Hugo wendet das Script
   [`themes/website-layout/layouts/ftpedia/ftptoc.html`](ftpedia-ftptoc-html)
   auf jede andere `.md`-Datei der _section_ `ftpedia/` an, wenn deren
   `layout: "ftptoc"` lautet.
   Für unsere Zwecke gibt es genau eine Datei mit diesem Attribut.
*  Hugo wendet das Script
   [`themes/website-layout/layouts/ftpedia/ftp-extras.html`](ftpedia-ftp-extras-html)
   auf jede andere `.md`-Datei der _section_ `ftpedia/` an, wenn deren
   `layout: "ftp-extras"` lautet.
   Für unsere Zwecke gibt es genau eine Datei mit diesem Attribut.
*  Hugo wendet das Script
   [`themes/website-layout/layouts/_default/file.html`](../knowhow/#download-datei-seitenansicht-file-html)
   auf jede andere `.md`-Datei (in jeder _Section_ übrigens) an, wenn deren
   `layout: "file"` lautet; jedoch nicht auf Dateien mit Namen `_index.md`.
*  Beim Erstellen (Neuanlegen) von Inhalten sind _Archetype_ behilflich.

