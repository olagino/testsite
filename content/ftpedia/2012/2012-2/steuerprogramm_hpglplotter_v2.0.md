---
layout: "file"
hidden: true
title: "Steuerprogramm HP-GL-Plotter v2.0"
date: "2012-06-30T00:00:00"
file: "steuerprogramm_hpglplotter_v2.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/steuerprogramm_hpglplotter_v2.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/steuerprogramm_hpglplotter_v2.0.zip -->
Steuerprogramm (siehe ft:pedia 2/2012) für den in ft:pedia 4/2011 vorgestellten HP-GL-Plotter (siehe auch [minimalistischer Präzisionsplotter](http://www.ftcommunity.de/categories.php?cat_id=2456) ). Änderung zu Version 1.1: Vier Parameter, sechs neue HP-GL-Kommandos).
