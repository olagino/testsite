---
layout: "comment"
hidden: true
title: "24172"
date: "2018-09-23T18:00:11"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Nein, nein, Stefan,
das ist keine Klingel! Der elektromechanische Teil ist zwar genauso aufgebaut, aber das hier ist das Erodier-Gerät aus der ft:pedia 2017-4.
Hinten rechts der grüne Kasten ist der von Bello et al. entwickelte, fantastische Akku-Kasten mit Kurzschlusschutz.