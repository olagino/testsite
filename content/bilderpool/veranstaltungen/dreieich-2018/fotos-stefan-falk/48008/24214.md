---
layout: "comment"
hidden: true
title: "24214"
date: "2018-10-02T01:35:32"
uploadBy:
- "TobiasBrunk3@gmx.de"
license: "unknown"
imported:
- "2019"
---
Die alte ,,Adler" Dampflok in neuer Form.
Sie dampft richtig im Zylindertakt und gibt echte Dampflokgeräusche von sich.