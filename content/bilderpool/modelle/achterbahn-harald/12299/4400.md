---
layout: "comment"
hidden: true
title: "4400"
date: "2007-10-24T16:50:51"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Oha, um die Gewichte habe ich mir gar keine Gedanken gemacht. Meine sind eingeclipst und haben ein wenig Spielraum, aber rausfallen tut da keines.


Nach den jüngsten Erkenntnissen (sprich: Crashs ...) ist es wohl besser, auf das Mittelgelenk und die inneren Achsen ganz zu verzichten. Ein langer Wagen mit den Rädern nur ganz hinten und ganz vorn dürfte doch stabiler in der Bahn bleiben. 

Gruß,
Harald