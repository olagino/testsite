---
layout: "comment"
hidden: true
title: "20260"
date: "2015-02-27T14:22:20"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo  Harry!

Das ist alles schön und gut. Nur ergibt das eine Bahn ohne längs liegende Winkelsteine, und mit ziemlich großen Kurvenradien. Ich habe ja schon überlegt, zwei Löcher in die Wand zwischen Wohn- und Bastelzimmer zu schlagen (eins oben, eins unten), aber eins hat mich davon abgehalten: das Teil muss auch noch ins Auto passen.

Gruß,
Harald