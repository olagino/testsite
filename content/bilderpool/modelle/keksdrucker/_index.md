---
layout: "overview"
title: "Keksdrucker"
date: 2020-02-22T08:40:38+01:00
legacy_id:
- /php/categories/3438
- /categories3c86.html
- /categories63f8.html
- /categories8b6c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3438 --> 
Weihnachten steht vor der Tür. Obwohl 3D Drucker schon in vielen fischertechnikerhaushalten stehen, müssen Kekse immer noch manuell verziert werden. Hier hilft Magic's Cookie Printer - die Fotos zeigen wie er funktioniert.