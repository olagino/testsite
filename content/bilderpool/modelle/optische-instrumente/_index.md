---
layout: "overview"
title: "Optische Instrumente"
date: 2020-02-22T08:40:39+01:00
legacy_id:
- /php/categories/3517
- /categories0ba3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3517 --> 
Hierher packen wir Mikroskope, Teleskope, und was sonst noch alles mit Optik zu tun hat.